### Contributing
Although I'm fairly sure I can pull this of, all help is appreciated. If you don't are the developing kind of person, please give your input in the [issues](https://gitlab.com/awesonium/minecraft.js/issues).

If you want to contribute code-wise, just fork the project and pull request after you made your changes.

Most of work for me is the node.js part of the app, (handling file downloads / caching / launching mc).

If you want more info or discuss your own ideas in person meet me [on Discord](https://discord.gg/0mcxjLzitV1s14iP)
/**
 * Created by l.heddendorp on 21.03.2016.
 */
import angular from 'angular'

import template from './packIndex.ng.html'

class IndexCtrl {
  constructor () {
    'ngInject';
  }
}

//this is shown is no pack was selected
let index = {
  restrict: 'E',
  bindings: {},
  template,
  controller: IndexCtrl,
  controllerAs: 'index'
};

export default angular
  .module('app.packs.index', [])
  .component('packIndex', index)
  .name

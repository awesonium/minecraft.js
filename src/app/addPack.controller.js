/**
 * Created by l.heddendorp on 25.03.2016.
 */

//controller to the pack adding dialog
import request from 'request'
export default class PackController {
  constructor ($mdDialog, $mdToast, packs) {
    'ngInject';
    this._request = request;
    this._toast = $mdToast;
    this._dialog = $mdDialog;
    this._packs = packs;
    //indicates if progressbar should be shown
    this.progress = false;
  }

  //helper function to display a toast
  _notify (text) {
    this._toast.showSimple(text)
  }

  //Gets the pack data from technic api and adds the pack
  add () {
    this.progress = true;
    this._request({
      url: this.url + '?build=99',
      method: 'GET',
      json: true,
      body: request
    }, (err, res, body) => {
      if (err) {
        console.warn(err);
        this._notify('An error occurred')
      } else if (body.error) {
        console.warn(res);
        console.warn(body);
        this._notify(body.error)
      } else {
        //Test if the pack is solder enabled
        if( body.solder )
        {
          this._notify('Fetch successful');
          this._packs.add(body);
          console.info(body);
          this.url = '';
          this.cancel()
        } else {
          this._notify('Only solder packs are supported');
          this.url = '';
        }
      }
      this.progress = false
    })
  }
  //Close the dialog
  cancel () {
    this._dialog.cancel()
  }
}

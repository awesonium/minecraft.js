/**
 * Created by l.heddendorp on 20.03.2016.
 */
import angular from 'angular'
import ngMaterial from 'angular-material'
import 'lokijs'
import 'lokijs/src/loki-indexed-adapter'
import lokiJS from 'lokijs/src/loki-angular'
//import remote from 'remote'
import '@angular/router/angular1/angular_1_router'
import 'angular-material/angular-material.scss'
import './../style/app.scss'

import packExplorer from './packs/packExplorer'
import mcAuthService from './services/minecraftAuth'
import progressService from './services/progress.service'
import packService from './services/packService'
import dbService from './services/db.service'
import settingsDialog from './settings/settings'
import solderLoader from './services/solderLoader.service'
import LoginCtrl from './login.controller'
import PackCtrl from './addPack.controller'
import theme from './app.theme'

class AppCtrl {
  constructor(mcAuth, $mdDialog, settings, packs, $rootRouter, sLoader) {
    'ngInject';
    //Assign injected services to the local variable
    this._router = $rootRouter;
    this._settings = settings;
    this._auth = mcAuth;
    this._dialog = $mdDialog;
    this._packs = packs;
    this._loader = sLoader;
    //Update the current packs
    packs.list().then((packs) => {
      this.packs = packs
    })
  }

  //passes on the seetings event for showing the dialog
  settings(event) {
    this._settings.show(event)
  }

  //Navigate to a pack as ngMaterial doesn't support the router links yet
  showPack(id) {
    this._router.navigate(['Packs', 'Show', {id: id}])
  }

  //Sends a delete request to the pack service and refreshes the list afterwards
  deletePack(pack) {
    this._packs.delete(pack);
    this._packs.list().then((packs) => {
      this.packs = packs
    })
  }

  //Opens the dialog for adding a pack, refreshes the pack list afterwards
  addPack(event) {
    this._dialog.show({
      controller: PackCtrl,
      controllerAs: 'pack',
      template: require('./addPack.ng.html'),
      parent: angular.element(document.body),
      targetEvent: event,
      clickOutsideToClose: true
    }).then(() => {
      this._packs.list().then((packs) => {
        this.packs = packs
      })
    })
  }

  //Passes on the logout action to the service
  logout() {
    this._auth.logout()
  }

  //Opens the dialog for logging in users
  login(event) {
    this._dialog.show({
      controller: LoginCtrl,
      controllerAs: 'login',
      template: require('./login.ng.html'),
      parent: angular.element(document.body),
      targetEvent: event,
      clickOutsideToClose: true
    })
  }
}

//Define the app component
let app = {
  template: require('./app.ng.html'),
  controller: AppCtrl,
  controllerAs: 'app',
  $routeConfig: [
    {path: '/packs/...', name: 'Packs', component: 'packs', useAsDefault: true}
  ]
};

//Get the logger from the main process
const electron = require('electron');
const remote = electron.remote;
const logger = remote.getGlobal('logger');

//Initialize the application
angular
  .module('app', [
    ngMaterial,
    lokiJS.name,
    'ngComponentRouter',
    dbService,
    solderLoader,
    mcAuthService,
    progressService,
    packService,
    settingsDialog,
    packExplorer
  ])
  .config(theme)
  .value('$routerRootComponent', 'app')
  .value('logger', logger)
  .component('app', app);

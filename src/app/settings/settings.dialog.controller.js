/**
 * Created by l.heddendorp on 23.03.2016.
 */

//Controller to the settings dialog
export default class SettingsCtrl {
  constructor ($mdDialog) {
    'ngInject';
    this._dialog = $mdDialog
  }
  cancel () {
    this._dialog.cancel()
  }
}

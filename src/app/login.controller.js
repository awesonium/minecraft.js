/**
 * Created by l.heddendorp on 22.03.2016.
 */

//Controller to the login dialog
export default class SettingsCtrl {
  constructor ($mdDialog, $mdToast, mcAuth) {
    'ngInject';
    this._toast = $mdToast.showSimple;
    this._auth = mcAuth;
    this._dialog = $mdDialog;
    //Indicates weather the progressbar is shown
    this.progress = false;
  }
  //handles the login
  login () {
    this.progress = true;
    this._auth.authenticate(this.username, this.password).then(
      //Successful login, clear progress and close dialog
      () => {
        this.progress = false;
        this.cancel()
      },
      //Failed login, hide progressbar and clear password, so the user can try again
      () => {
        this.progress = false;
        this.password = ''
      }
    )
  }
  cancel () {
    //Cancel login, clear fields for the next call
    this.username = '';
    this.password = '';
    this._dialog.cancel()
  }
}
